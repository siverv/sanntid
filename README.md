
# Sanntid Rutetabell

## Realtime Timetable for Norwegian Collective Transport

The convenience of the long-missed 'Ruter Sanntid'-app for all of the Norwegian collective transport network, through Entur's open API.

It is currently limiting itself to one automatic request for departures per 30 seconds, with a strict limit of one request per 5 seconds in general. Multiple requests within 5 seconds will be batched up and called together after waiting.

[Public page](https://siverv.codeberg.page/sanntid)

## Setup

1. Read up on [authentication for Entur's Open API](https://developer.entur.org/stop-places-v1-read#authentication)
2. Change the value of `VITE_SANNTID_ENTUR_CLIENT_NAME` in [the .env-file](./env)
3. Run `npm ci --ignore-scripts`
4. Run `npm start` for dev-mode and `npm run build && npm run serve` for production.

## Credits

- [Entur's API](https://developer.entur.org/pages-journeyplanner-journeyplanner-v2) for data about the Norwegian collective transport network, including realtime estimates for arrival.
- [Solid.js](https://www.solidjs.com/) is a neat and really lightweight framework alternative to React/Preact. JSX and reactivity without VDOM.
- Build and PWA with [Vite](https://github.com/vitejs/vite)
- Linted with [ESLint](https://eslint.org/)
- Programmed mostly with [Sublime Text](https://www.sublimetext.com/)