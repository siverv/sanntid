

## Main directories:

- src/
  - Pre-app specific plumbing, regarding vite, solid, or similar.
- src/app:
  - Global app components, state, style, plumbing.
- src/pages:
  - Page-specific components, state, style, plumbing
- src/utils:
  - Common and completely app-agnostic components, functions, classes, constants.
- src/assets:
  - Static assets like images, favicons, fonts, etc.


## Index.jsx/index.js

The index-files should export exactly those concepts which are relevant outside of a given directory.
The index-files should minimize any actual logic, and concern itself mostly by stitching together the content of its directory into a usable whole.
Content from sub-directories should be handled by their own directories' index-files.


E.g

- src/index.jsx:
  - entry-point for vite
- src/app/index.jsx:
  - The App to be used in the the entry-point.
- src/app/state/index.jsx:
  - The app-state exports relevant outside of of src/app/state
- src/pages/index.jsx:
  - The routes, to be used in App. The routes can be defined in a routes.jsx if requiring logic or large amount of code.
- src/pages/somePage/index.jsx: 
  - The page to be used in the routes.


Unless the main content is in files of their own, index-files should probably not be used.



src/app/components/index.jsx and src/utils/index.jsx should not be made.
These would be boilerplate catch-alls that are bothersome to maintain, and of less usability than going straight to the files one wants.

However, src/app/components/ui/index.jsx could be relevant, as a collection of common ui-components, of which one often wants many at once.