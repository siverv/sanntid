import { createSignal } from 'solid-js';

export const [debugLevel, setDebugLevel] = createSignal(parseInt(import.meta.env.VITE_DEBUG_LEVEL) || 0);
export const [getLog, setLog] = createSignal([]);
export const addDebugLine = (line, level = 1) => {
  if(debugLevel() >= level){
    let timestamp = new Date().toISOString();
    setLog(getLog().concat(`${timestamp}: ${line}`).slice(-1000));
  }
};


if(debugLevel() > 5){
  let oldLog = console.log;
  console.log = function(line){
    setTimeout(( )=> addDebugLine(line), 1000);
    oldLog.apply(this, arguments);
  };
  let oldWarn = console.warn;
  console.warn = function(line){
    setTimeout(( )=> addDebugLine(line, 2), 1000);
    oldWarn.apply(this, arguments);
  };
  let oldError = console.error;
  console.error = function(line){
    setTimeout(( )=> addDebugLine(line, 3), 1000);
    oldError.apply(this, arguments);
  };
}


export function DebugLevelInput(){
  return <label>
    <input type="checkbox" checked={debugLevel() !== 0} onChange={ev => {
      setDebugLevel(ev.target.checked ? 1 : 0);
      if(!ev.target.checked){
        setLog([]);
      }
    }}/>
    Debug
  </label>;
}


export function DebugLog(){
  return <Show when={debugLevel() > 0}>
    <ul>
      <For each={getLog()}>
        {line => <li><pre>{line}</pre></li>}
      </For>
    </ul>
  </Show>;
}