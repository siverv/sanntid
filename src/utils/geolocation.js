import { createSignal, batch, useContext, createContext } from "solid-js";


const LEAST_TIME_BEFORE_RENEWING_GEOLOCATION = 1000;

function shouldRenew(timestamp){
  return Date.now() - timestamp > LEAST_TIME_BEFORE_RENEWING_GEOLOCATION;
}

export const GeoContext = createContext();

export function useGeolocation(force){
  const signals = useContext(GeoContext);
  return getGeolocation(force, signals);
}

export function createGeoSignals(){
  return {
    timestamp: createSignal(false),
    permission: createSignal(null),
    coords: createSignal(null),
    error: createSignal(null)
  };
}

function getGeolocation(force, signals){
  let {
    timestamp: [timestamp, setTimestamp],
    permission: [permission, setPermission],
    error: [error, setError],
    coords: [coords, setCoords]
  } = signals;
  const geolocation = {
    timestamp,
    permission,
    error,
    coords,
    refetch: () => getGeolocation(true, signals)
  };
  if(!force && !shouldRenew(timestamp())) {
    return geolocation;
  }
  if(!navigator.geolocation){
    batch(() => {
      setTimestamp(Date.now());
      setError(null);
      setPermission(false);
    });
    return;
  }
  navigator.geolocation.getCurrentPosition(
    (position) => {
      let {latitude, longitude} = position.coords;
      batch(() => {
        setTimestamp(Date.now());
        setError(null);
        setCoords({latitude, longitude});
      });
    },
    (error) => {
      batch(() => {
        setTimestamp(Date.now());
        setError(error);
      });
    }
  );
  navigator.permissions?.query({ name: 'geolocation' })
    .then(setPermission);
  return geolocation;
}