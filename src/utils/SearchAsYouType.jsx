import { createResource, createSignal } from "solid-js";
import { getMessage } from "../app/i18n/i18n";

function wait(millis){
  return new Promise(resolve => setTimeout(resolve, millis));
}

export function SearchAsYouType({minDelay, Item, search}){
  if(minDelay === undefined){
    minDelay = 1000;
  }
  const [query, setQuery] = createSignal("");
  const [nextQuery, setNextQuery] = createSignal(null);
  const [suggestions] = createResource(query, search, {initialValue:[]});
  async function handleQueryChange(value){
    if(nextQuery() || suggestions.loading){
      setNextQuery(value);
      return;
    }
    setQuery(value);
    await wait(minDelay);
    let next = nextQuery();
    if(next){
      setNextQuery(null);
      setQuery(next);
    }
  }
  return <div>
    <label>
      {getMessage("search.inputLabel")}
      <input type="name" onInput={(ev) => handleQueryChange(ev.target.value)}/>
    </label>
    <ul>
      <For each={suggestions()} fallback={<></>}>
        {(item) => <Item item={item}/>}
      </For>
      <Show when={suggestions.loading}>
        <i>
          {getMessage("search.loadingSuggestions")}
        </i>
      </Show>
    </ul>
  </div>;
}
