import { createSignal, observable, onCleanup } from 'solid-js';

const SOFT_REMOVED = Symbol("SOFT REMOVED");

export function hasId(id, [getMap]){
  let item = getMap().get(id);
  return item && !item[SOFT_REMOVED];
}

export function softRemove(id, [getMap, setMap]){
  let item = getMap().get(id);
  if(item && !item[SOFT_REMOVED]){
    setMap(new Map(getMap()).set(id, {
      ...item,
      [SOFT_REMOVED]: true
    }));
  }
}

export function commitAllSoftRemoved([getMap, setMap]){
  let map = getMap();
  let newMap = new Map(map);
  for(let [id,item] of newMap){
    if(item[SOFT_REMOVED]){
      newMap.delete(id);
    }
  }
  if(newMap.size < map.size) {
    setMap(newMap);
  }
}

export function undoSoftRemove(id, [getMap, setMap]){
  let item = getMap().get(id);
  if(item && item[SOFT_REMOVED]){
    let newQuay = {...item};
    delete newQuay[SOFT_REMOVED];
    setMap(new Map(getMap()).set(id, newQuay));
  }
}

export function moveById(id, toIndex, [getMap, setMap]){
  let mapList = Array.from(getMap());
  let index = mapList.findIndex(entry => entry[0] === id);
  if(index >= 0 && toIndex >= 0 && toIndex < mapList.length){
    let [entry] = mapList.splice(index, 1);
    mapList.splice(Math.max(0, Math.min(mapList.length, toIndex)), 0, entry);
    setMap(new Map(mapList));
  }
}

function mapToJson(map){
  let list = [...map].filter(([_,v]) => !v[SOFT_REMOVED]);
  return JSON.stringify(list);
}

function mapFromJson(key, json){
  try {
    return new Map(JSON.parse(json));
  } catch (error) {
    console.warn("Could not create map collection from stored json with key: " + key, {key, json});
  }
}

function autoSaveMapSignal(key, [getMap]){
  const subscription = observable(getMap).subscribe((quayMap) => {
    if(!quayMap || quayMap.size === 0){
      localStorage.removeItem(key);
    } else {
      localStorage.setItem(key, mapToJson(quayMap));
    }
  });
  onCleanup(() => {
    subscription.unsubscribe();
  });
  return subscription;
}

export function createLocalStorageMapSignal(key){
  let storedJson = localStorage.getItem(key);
  let initialMap = storedJson ? mapFromJson(key, storedJson) : new Map();
  const signal = createSignal(initialMap);
  autoSaveMapSignal(key, signal);
  return signal;
}