import { createMemo, createSignal, Match, Switch, onCleanup } from "solid-js";
import { getMessage, getRelativeTimeFormat } from "../app/i18n/i18n";
import { useConfig } from '../app/state';

const [onTick, tick] = createSignal(undefined, {equals: false});
const TICK_INTERVAL = 10 * 1000;
let updateIntervalId = null;

document.addEventListener("visibilitychange", () => {
  if(document.hidden){
    clearTimeout(updateIntervalId);
    updateIntervalId = null;
  } else if(updateIntervalId == null) {
    updateIntervalId = setInterval(tick, TICK_INTERVAL);
  }
}, false);


export function getClockFace(dateTime){
  let h = dateTime.getHours().toString().padStart(2, "0");
  let m = dateTime.getMinutes().toString().padStart(2, "0");
  return `${h}:${m}`;
}

export function getClockFaceWithSeconds(dateTime){
  let h = dateTime.getHours().toString().padStart(2, "0");
  let m = dateTime.getMinutes().toString().padStart(2, "0");
  let s = dateTime.getSeconds().toString().padStart(2, "0");
  return `${h}:${m}:${s}`;
}
export function RelativeToNow({then, reverse}){
  const [{showExactTime}] = useConfig();
  let minutes = createMemo(() => {
    onTick();
    return Math.round((Date.now() - then)/60000) * (reverse ? 1 : -1);
  });
  return <Switch>
    <Match when={showExactTime()}>
      {getClockFaceWithSeconds(then)}
    </Match>
    <Match when={Math.abs(minutes()) < 1}>
      {getMessage("common.datetime.now")}
    </Match>
    <Match when={Math.abs(minutes()) <= 15}>
      {getRelativeTimeFormat().format(minutes(), "minutes")}
    </Match>
    <Match when={Math.abs(minutes()) > 15}>
      {getClockFace(then)}
    </Match>
  </Switch>;
}


export function createTimer(intervalMs){
  const [getNow, setNow] = createSignal(Date.now());
  let intervalId;
  intervalId = setInterval(() => setNow(Date.now()), intervalMs);
  onCleanup(() => {
    clearInterval(intervalId);
  });
  return getNow;
}

function percentToAngularPosition(cx, cy, radius, percent) {
  return {
    x: cx + (radius * Math.cos(percent * 2 * Math.PI)),
    y: cy + (radius * Math.sin(percent * 2 * Math.PI))
  };
}

export function Countdown({getWhen, maxMs}){
  const getNow = createTimer(1000);
  const timeLeftMs = createMemo(() => {
    return Math.max(getWhen() - getNow(), 0);
  });
  let cx = 100;
  let cy = 100;
  let radius = 85;
  let startPercent = 0.75;
  let start = percentToAngularPosition(cx,cy,radius,startPercent);
  const getPiePath = (() => {
    let percent = 1- Math.min(timeLeftMs() / maxMs, 1);
    let endPercent = (percent + startPercent) % 1;
    let end = percentToAngularPosition(cx,cy,radius,endPercent);
    return `M ${cx} ${cy} L ${start.x} ${start.y} A ${radius} ${radius} 1 ${percent < 0.5 ? 1 : 0} 0 ${end.x} ${end.y}`;
    // return `M ${cx} ${cy} L ${start.x} ${start.y} L ${end.x} ${end.y}`;
  });
  return <svg class="countdown" viewBox="0 0 200 200">
    <path d={getPiePath()} stroke="none" fill="#0002"/>
  </svg>;
}