import { createSignal } from "solid-js";

import messages_nb_no from './nb.js';
import messages_en from './en.js';


export const languages = new Map()
  .set("nb-NO", "Norsk (bokmål)")
  .set("en", "English");

let initialLanguage = localStorage.getItem("userPreferredLanguage");
if(!languages.has(initialLanguage)){
  initialLanguage = "nb-NO";
}

export const languageSignal = createSignal(initialLanguage);
export const [language, setLanguage] = languageSignal;

const messagesMap = new Map();
messagesMap.set("nb-NO", messages_nb_no);
messagesMap.set("en", messages_en);

export function getMessage(id, lang=language()){
  let map = messagesMap.get(lang);
  let text = map?.get(id);
  if(!text){
    console.warn(`No label in language '${lang}' for id: ${id}`);
    text = Array.from(messagesMap.values()).find(map => map.get(id));
  }
  return text;
}

export function getRelativeTimeFormat(){
  return new Intl.RelativeTimeFormat(language(), {
    localeMatcher: "best fit",
    numeric: "always",
    style: "short",
  });
}
