

const messages = new Map();

messages.set("header.title", "Realtime Timetable");
messages.set("page.saved", "Favourites");
messages.set("page.nearby", "Nearby");
messages.set("page.search", "Search");
messages.set("page.settings", "Settings");
messages.set("page.selected", "Selected routes");
messages.set("footer.code", "code");
messages.set("footer.data", "data");

messages.set("common.loading", "Loading...");
messages.set("common.datetime.now", "Now");
messages.set("common.clearData", "Clear data");

messages.set("geolocation.denied", "The application does not have access to your location");
messages.set("geolocation.error", "Locationproblems");
messages.set("geolocation.lat", "Lat");
messages.set("geolocation.long", "Long");
messages.set("geolocation.lastUpdated", "Location last updated");
messages.set("geolocation.refresh", "Update location");
messages.set("geolocation.loading", "Loading geolocation");

messages.set("entur.transportMode.metro", "metro");
messages.set("entur.transportMode.bus", "bus");
messages.set("entur.transportMode.rail", "rail");
messages.set("entur.transportMode.tram", "tram");

messages.set("quayMap.quayStar.toggle.isSaved", "Remove from favourites");
messages.set("quayMap.quayStar.toggle.isNotSaved", "Mark as favourite");
messages.set("quayMap.quayStar.checkbox", "Favourite");
messages.set("quayMap.loadingDepartures", "Loading departures");
messages.set("quayMap.lastUpdated.loading", "Updating..");
messages.set("quayMap.lastUpdated.neverBefore", "No timetables loaded");
messages.set("quayMap.lastUpdated.lastUpdated", "Timetables last updated");
messages.set("quayMap.lastUpdated.refresh", "Update timetables");
messages.set("quayMap.line.noDepartures", "No departures");
messages.set("quayMap.line.delay", "Delay");
messages.set("quayMap.line.showQuay", "Show other routes from here");
messages.set("quayMap.line.hideQuay", "Hide other routes");
messages.set("quayMap.error.fetching", "Could not load timetables");
messages.set("quayMap.noLineDepartingWithinFiveHours", "No departures within the next 5 hours");

messages.set("saved.modify", "Modify favourites");
messages.set("saved.back", "Back to favourites");
messages.set("saved.share", "Share favourites by link");

messages.set("selected.share", "Share by link");

messages.set("nearby.loadNearby", "Load nearby venues");

messages.set("search.inputLabel", "Search venues");
messages.set("search.loadingSuggestions", "Loading suggestions...");

messages.set("settings.header", "Settings");
messages.set("settings.preferences", "Preferences");
messages.set("settings.geolocation", "Location");
messages.set("settings.system", "System");
messages.set("settings.clearPermanentStorage", "Clear permanent storage");
messages.set("settings.clearPermanentStorage.confirm", "Are you certain you want to clear the permanent storage, including the favourites?");
messages.set("settings.selectPreferredLanguage", "Preferred language");
messages.set("settings.autoTick", "Continuous updating of timetables");
messages.set("settings.showExactTime", "Show exact time");
messages.set("settings.showAllLines", "Show lines without departures within the next 5 hours");
messages.set("settings.hardReload", "Reload page");
messages.set("settings.export", "Export Settings");
messages.set("settings.export.button", "Export settings");
messages.set("settings.import", "Import Settings");
messages.set("settings.import.button", "Import settings");
messages.set("settings.saved", "Favourites");
messages.set("settings.departures", "About the timetable");
messages.set("settings.departures.realtime", "Real time");
messages.set("settings.departures.static", "Table time");
messages.set("settings.departures.delayed", "Delayed (> 3 min)");

messages.set("modifySaved.up", "Up");
messages.set("modifySaved.down", "Down");
messages.set("modifySaved.remove", "Remove");
messages.set("modifySaved.restore", "Restore");


export default messages;
