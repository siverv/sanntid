

const messages = new Map();

messages.set("header.title", "Sanntid Rutetabell");
messages.set("page.saved", "Favoritter");
messages.set("page.nearby", "Nære");
messages.set("page.search", "Søk");
messages.set("page.settings", "Innstillinger");
messages.set("page.selected", "Valgte ruter");
messages.set("footer.code", "kode");
messages.set("footer.data", "data");

messages.set("common.loading", "Laster...");
messages.set("common.datetime.now", "Nå");
messages.set("common.clearData", "Tøm listen");

messages.set("geolocation.denied", "Applikasjonen har ikke tilgang til din posisjon");
messages.set("geolocation.error", "Posisjonsproblemer");
messages.set("geolocation.lat", "Lat");
messages.set("geolocation.long", "Long");
messages.set("geolocation.lastUpdated", "Posisjon sist oppdatert");
messages.set("geolocation.refresh", "Oppdater posisjon");
messages.set("geolocation.loading", "Laster inn posisjon");

messages.set("entur.transportMode.metro", "t-bane");
messages.set("entur.transportMode.bus", "buss");
messages.set("entur.transportMode.rail", "tog");
messages.set("entur.transportMode.tram", "trikk");

messages.set("quayMap.quayStar.toggle.isSaved", "Fjern fra favoritter");
messages.set("quayMap.quayStar.toggle.isNotSaved", "Sett som favoritt");
messages.set("quayMap.quayStar.checkbox", "Favoritt");
messages.set("quayMap.loadingDepartures", "Laster inn avganger");
messages.set("quayMap.lastUpdated.loading", "Oppdaterer....");
messages.set("quayMap.lastUpdated.neverBefore", "Ingen rutetider lastet inn");
messages.set("quayMap.lastUpdated.lastUpdated", "Rutetider sist oppdatert");
messages.set("quayMap.lastUpdated.refresh", "Oppdater rutetider");
messages.set("quayMap.line.noDepartures", "Ingen avganger");
messages.set("quayMap.line.delay", "Forsinkelse");
messages.set("quayMap.line.showQuay", "Vis andre ruter fra her");
messages.set("quayMap.line.hideQuay", "Skjul andre ruter");
messages.set("quayMap.error.fetching", "Kunne ikke laste rutetider");
messages.set("quayMap.noLineDepartingWithinFiveHours", "Ingen avganger innen de neste 5 timene");

messages.set("saved.modify", "Endre favoritter");
messages.set("saved.back", "Tilbake til favoritter");
messages.set("saved.share", "Del favorittene via lenke");

messages.set("selected.share", "Del via lenke");

messages.set("nearby.loadNearby", "Hent stoppesteder nær deg");

messages.set("search.inputLabel", "Søk etter stoppesteder");
messages.set("search.loadingSuggestions", "Laster inn forslag");

messages.set("settings.header", "Innstillinger");
messages.set("settings.preferences", "Valg");
messages.set("settings.geolocation", "Posisjon");
messages.set("settings.system", "System");
messages.set("settings.clearPermanentStorage", "Slett lagrede data");
messages.set("settings.clearPermanentStorage.confirm", "Er du sikker på at ønsker å slette alle lokalt lagrede data, inkludert favorittene?");
messages.set("settings.selectPreferredLanguage", "Foretrukket språk");
messages.set("settings.autoTick", "Kontinuerlig oppdatering av rutetider");
messages.set("settings.showExactTime", "Vis eksakt tid");
messages.set("settings.showAllLines", "Vis linjer uten avganger innen de neste 5 timene");
messages.set("settings.hardReload", "Gjeninnlast siden");
messages.set("settings.export", "Eksporter innstillinger");
messages.set("settings.export.button", "Eksporter innstillinger");
messages.set("settings.import", "Importer innstillinger");
messages.set("settings.import.button", "Importer innstillinger");
messages.set("settings.saved", "Favoritter");
messages.set("settings.departures", "Om rutetidene");
messages.set("settings.departures.realtime", "Sanntid");
messages.set("settings.departures.static", "Rutetid");
messages.set("settings.departures.delayed", "Forsinket (> 3 min)");

messages.set("modifySaved.up", "Opp");
messages.set("modifySaved.down", "Ned");
messages.set("modifySaved.remove", "Fjern");
messages.set("modifySaved.restore", "Legg til");

export default messages;
