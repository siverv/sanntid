import { createEffect, onCleanup } from "solid-js";
import { getMessage } from "./i18n/i18n";
import { useLocation } from "@solidjs/router";
import { commitAllSoftRemoved } from "../utils/mapSignal";
import { DepartureContext, createDepartureStore } from './state/departures';
import { SavedContext, createSaved } from './state/saved';
import { ConfigContext, createConfig } from './state/config';
import { Layout } from './Layout';
import { GeoContext, createGeoSignals } from "../utils/geolocation";


function updateTitleOnPageChange(){
  const location = useLocation();
  createEffect(() => {
    let pageId = location.pathname.slice(1);
    if(pageId){
      document.title = `${getMessage(`page.${pageId}`)} - ${getMessage("header.title")}`;
    } else {
      document.title = `${getMessage("header.title")}`;
    }
  });
}

function saveSoftCommitedChangesOnPageChange(saved){
  const commit = () => commitAllSoftRemoved(saved);
  window.addEventListener("hashchange", commit);
  onCleanup(() => window.removeEventListener("hashchange", commit));
}

export function App(props) {
  const config = createConfig();
  const departureStore = createDepartureStore(config);
  const saved = createSaved();
  const geoSignals = createGeoSignals();
  updateTitleOnPageChange();
  saveSoftCommitedChangesOnPageChange(saved);
  return <>
    <DepartureContext.Provider value={departureStore}>
      <ConfigContext.Provider value={config}>
        <SavedContext.Provider value={saved}>
          <GeoContext.Provider value={geoSignals}>
            <Layout>
              {props.children}
            </Layout>
          </GeoContext.Provider>
        </SavedContext.Provider>
      </ConfigContext.Provider>
    </DepartureContext.Provider>
  </>;
}
