
import { createSignal, useContext, createContext } from "solid-js";

export const ConfigContext = createContext();

export function useConfig(){
  return useContext(ConfigContext);
}

export function createConfig(){
  const [hasAutoTick, setAutoTick] = createBooleanSetting("autoTick", true);
  const [showExactTime, setShowExactTime] = createBooleanSetting("showExactTime", false);
  const [showAllLines, setShowAllLines] = createBooleanSetting("showAllLines", false);
  return [
    {hasAutoTick, showExactTime, showAllLines},
    {setAutoTick, setShowExactTime, setShowAllLines}
  ];
}

function createBooleanSetting(key, defaultValue){
  let stored = localStorage.getItem(key);
  const [get, _set] = createSignal(stored != null ? stored : defaultValue);
  function set(value){
    if(value){
      localStorage.setItem(key, true);
    } else {
      localStorage.removeItem(key);
    }
    _set(value);
  }
  return [get, set];
}