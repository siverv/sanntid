

import { createContext, useContext, createMemo } from "solid-js";

const DELIM = "__";
export function joinId(quayId, lineId, name){
  return [quayId].concat(lineId||[]).concat(name||[]).join(DELIM);
}
export function splitId(id){
  return id.split(DELIM);
}

export const QuayMapContext = createContext([]);

export function useQuayMap(){
  return useContext(QuayMapContext);
}

export function createQuayStore(departureStore, quayMapSignal){
  const [quayMap, setQuayMap] = quayMapSignal;
  const [_, {getTimeMap, visibleWhileMounted}] = departureStore;
  const lastUpdated = createMemo(() => {
    let timeMap = getTimeMap();
    let updatedTimes = Array.from(quayMap().keys()).map(quayId => timeMap.get(quayId)).filter(Boolean);
    let earliestUpdatedTime = Math.min(...updatedTimes);
    return new Date(earliestUpdatedTime);
  }); 
  visibleWhileMounted(quayMap);
  return [quayMap, setQuayMap, lastUpdated];
}