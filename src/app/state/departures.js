
// eslint-disable-next-line import/named
import { createResource, createSignal, createContext, observable, onCleanup, onMount, useContext } from "solid-js";
import { getDeparturesFromQuayIds } from "../api/entur";
import { splitId } from "./quays";

export const DepartureContext = createContext([]);

export const SUFFICIENTLY_LONG_AGO = 30 * 1000;

export const TRY_RETCHING_EVERY = (SUFFICIENTLY_LONG_AGO / 2) + 500;

export function useDepartureStore(){
  return useContext(DepartureContext);
}

export function createDepartureStore(config){
  const [{hasAutoTick}] = config;
  function sufficientlyLongAgo(time){
    return (Date.now() - time) >= SUFFICIENTLY_LONG_AGO;
  }
  const [onTick, tick] = createSignal(undefined, {equals: false});
  let refetchDeparturesIntervalId = null;
  let lastTick = new Date();
  function startTicking(){
    if(!hasAutoTick()){
      return;
    }
    if(refetchDeparturesIntervalId == null) {
      refetchDeparturesIntervalId = setInterval(() => {
        lastTick = new Date();
        tick();
      }, TRY_RETCHING_EVERY);
      if(Date.now() - lastTick > SUFFICIENTLY_LONG_AGO){
        lastTick = new Date();
        tick();
      }
    }
  }
  function stopTicking(){
    clearTimeout(refetchDeparturesIntervalId);
    refetchDeparturesIntervalId = null;
  }

  function autoStopAutoTick(){
    let subscription = observable(hasAutoTick).subscribe((autoTick) => {
      if(autoTick){
        startTicking();
      } else {
        stopTicking();
      }
    });
    onCleanup(() => {
      subscription.unsubscribe();
    });
  }
  autoStopAutoTick();
  const [getTimeMap, setTimeMap] = createSignal(new Map());
  const [error, setError] = createSignal(null);
  const [visibleQuayMaps, setVisibleQuayMaps] = createSignal(new Set());
  const [quayLineDepartureMap, {refetch}] = createResource(visibleQuayMaps, async (quayMapSet, {value: previousValue}) => {
    let quayMaps = Array.from(quayMapSet);
    let allIds = quayMaps.flatMap(quayMap => Array.from(quayMap().keys()));
    let quayIdSet = Array.from(new Set(
      allIds.map(id => {
        let [quayId] = splitId(id);
        return quayId;
      })
    ));
    let map = new Map(previousValue);
    let oldTimeMap = getTimeMap();
    let idsToLoad = Array.from(quayIdSet).filter(id =>
      sufficientlyLongAgo(oldTimeMap.get(id) || 0)
    );
    if(idsToLoad.length === 0){
      return previousValue;
    }
    let now = new Date();
    setTimeMap(new Map([
      ...oldTimeMap,
      ...quayIdSet.filter(id => idsToLoad.includes(splitId(id)[0])).map(id => [id, now])
    ]));

    let newMap;
    try {
      newMap = await getDeparturesFromQuayIds(idsToLoad);
    } catch(err){
      setError(err);
      return previousValue;
    }
    setError(null);
    if(newMap){
      map = new Map([
        ...map,
        ...newMap
      ]);
    }
    return map;
  }, {
    initialValue: new Map()
  });
  const departureStore = [
    quayLineDepartureMap,
    {
      getTimeMap,
      refetch,
      error,
      visibleWhileMounted: (quayMap) => {
        onMount(() => setVisibleQuayMaps(new Set(visibleQuayMaps()).add(quayMap)));
        let subscription = observable(quayMap).subscribe(() => refetch());
        onCleanup(() => {
          subscription.unsubscribe();
          let set = new Set(visibleQuayMaps());
          set.delete(quayMap);
          setVisibleQuayMaps(set);
        });
      }
    }
  ];

  function toggleTickOnVisibility(){
    if(document.hidden){
      stopTicking();
    } else {
      startTicking();
    }
  }
  let subscription = observable(onTick).subscribe(() => {
    refetch();
  });
  document.addEventListener("visibilitychange", toggleTickOnVisibility, false);
  onMount(() => {
    startTicking();
  });
  onCleanup(() => {
    document.removeEventListener("visibilitychange", toggleTickOnVisibility);
    subscription.unsubscribe();
    stopTicking();
  });
  return departureStore;
}
