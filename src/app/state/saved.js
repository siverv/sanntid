import { joinId } from "./quays";
import { createLocalStorageMapSignal, hasId, moveById, softRemove, undoSoftRemove } from "../../utils/mapSignal";
import { createContext, useContext } from 'solid-js';

export const SavedContext = createContext();

export function useSaved(){
  return useContext(SavedContext);
}

export function createSaved(){
  const savedQuayMapSignal = createLocalStorageMapSignal("favourites");
  const [getSaved, setSaved] = savedQuayMapSignal;

  function isSaved(quayId, lineId, name) {
    return hasId(joinId(quayId, lineId, name), savedQuayMapSignal);
  }
  function toggleSavedById(id, quay){
    if(hasId(id, savedQuayMapSignal)){
      softRemove(id, savedQuayMapSignal);
    } else if(getSaved().has(id)){
      undoSoftRemove(id, savedQuayMapSignal);
    } else {
      setSaved(new Map(getSaved()).set(id, quay));
    }
  }
  function toggleSaved(quay, lineId, name){
    toggleSavedById(joinId(quay.id, lineId, name), quay);
  }
  function moveSaved(id, toIndex){
    return moveById(id, toIndex, savedQuayMapSignal);
  }
  function clearAllSaved(){
    setSaved(new Map());
  }

  return [getSaved, setSaved, {
    isSaved,
    toggleSavedById,
    toggleSaved,
    moveSaved,
    clearAllSaved
  }];
}