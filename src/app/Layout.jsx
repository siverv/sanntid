import { DebugLog } from "../utils/DebugLog";
import { createSignal, onCleanup } from 'solid-js';
import { getMessage } from "./i18n/i18n";
import { updateReady } from "../pwa";
import { A } from "@solidjs/router";
import logo from '../assets/favicon.svg?raw';

export function Layout({children}) {
  return <>
    <header class="main-header">
      <div class="inner">
        <span class="header-icon">
          <ClockLogo/>
        </span>
        <div class="header-main">
          <h1 class="header-title">
            {getMessage("header.title")}
          </h1>
          <nav class="header-navigation">
            <A class="nav-link" href="/saved">{getMessage(`page.saved`)}</A>
            <A class="nav-link" href="/nearby">{getMessage(`page.nearby`)}</A>
            <A class="nav-link" href="/search">{getMessage(`page.search`)}</A>
            <span class="spacer"/>
            <Show when={updateReady()}>
              <div class="new-version">
                New version is available. <button onClick={() => updateReady().applyUpdate()}>update</button>
              </div>
            </Show>
            <A class="nav-link" href="/settings">{getMessage(`page.settings`)}</A>
          </nav>
        </div>
      </div>
    </header>
    <main>
      <ErrorBoundary fallback={(error, reset) => {
        console.error(error);
        return <section>
          <h4>
            Oops. Something went wrong.
          </h4>
          <button onClick={reset}>
            Reset?
          </button>
          <pre>
            {error}
          </pre>
          <pre>
            {error.toString()}
          </pre>
        </section>;
      }}>
        {children}
      </ErrorBoundary>
    </main>
    <DebugLog/>
    <footer class="main-footer">
      <a href="https://siverv.no">
        siverv
      </a>
      {` | `} 
      <span>2021➾</span>
      {` | `} 
      <a href={import.meta.env.VITE_SANNTID_CODE_URL || "#not-yet-hosted"} class="source-code-link">
        {getMessage("footer.code")}
      </a>
      {` | `} 
      <a href="https://developer.entur.org/" class="source-code-link">
        {getMessage("footer.data")}
      </a>
      <br/>
      <span>v{/* eslint-disable-line no-undef */__APP_VERSION__}</span>
      {` | `} 
      <span>#{/* eslint-disable-line no-undef */__COMMIT_HASH__}</span>
    </footer>
  </>;
}


function ClockLogo(){
  let parser = new DOMParser();
  let svg = parser.parseFromString(logo, "image/svg+xml");
  let hourElm = svg.getElementsByClassName("h1")?.[0];
  let minutesElm = svg.getElementsByClassName("h2")?.[0];
  const updateSvg = (logo, dateTime=new Date()) => {
    let hour = (dateTime.getHours() % 12) / 12 * 360;
    hourElm.setAttribute("transform", `rotate(${hour})`);
    let minutes = dateTime.getMinutes() / 60 * 360;
    minutesElm.setAttribute("transform", `rotate(${minutes})`);
    return new XMLSerializer().serializeToString(svg);
  };
  const [logoSvg, setLogoSvg] = createSignal(updateSvg(logo));
  const intervalId = setInterval(() => setLogoSvg(updateSvg(logo)), 60 * 1000);
  onCleanup(() => {
    clearInterval(intervalId);
  });
  window.setDateTime = (dateTime) => setLogoSvg(updateSvg(logo, dateTime));
  return <img src={`data:image/svg+xml;base64,${btoa(logoSvg())}`} alt="" class="logo"/>;
}