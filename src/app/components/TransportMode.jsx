import { getMessage } from "../i18n/i18n.js";

export function TransportMode({mode}){
  return <span class="mode">
    [{getMessage(`entur.transportMode.${mode}`) || mode}]
  </span>;
}