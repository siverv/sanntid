import { useDepartureStore, useConfig } from '../state';
import { getMessage } from "../i18n/i18n.js";
import { QuayLineDepartures } from "./QuayLineDepartures.jsx";
import { Star } from "./QuayStar.jsx";
import { TransportMode } from './TransportMode';
import "./Quay.css";


export function QuayName({quay}){
  let modes = new Set(quay.lines.flatMap(line => line.transportMode));
  return <>
    {quay.name}
    {quay.publicCode ? ` (${quay.publicCode}) ` : null}
    <Show when={modes.size > 0}>
      <span> - </span>
      <For each={Array.from(modes)}>
        {mode => <TransportMode mode={mode}/>}
      </For>
    </Show>
    {quay.description ? ` - ${quay.description} ` : null}
  </>;
}


export function Quay({quay}){
  let [quayLineDepartureMap] = useDepartureStore();
  let [{showAllLines}] = useConfig();
  let getLines = (lineMap) => {
    if(showAllLines()){
      return quay.lines;
    } else {
      return quay.lines.filter(line => {
        return lineMap?.get(line.id);
      });
    } 
  };
  return <fieldset class="quay">
    <legend id={quay.id}>
      <a href={`/selected?id=${encodeURIComponent(quay.id)}`} class="quay-link">
        <QuayName quay={quay}/>
      </a>
      <Star quay={quay}/>
    </legend>
    <Switch>
      <Match when={!quayLineDepartureMap().get(quay.id)}>
        {getMessage("quayMap.loadingDepartures")}
      </Match>
      <Match when={quayLineDepartureMap().get(quay.id)}>
        {() =>
          <For each={getLines(quayLineDepartureMap().get(quay.id))} fallback={
            <span>
              {getMessage("quayMap.noLineDepartingWithinFiveHours")}
            </span>
          }>
            {line => <QuayLineDepartures line={line} quay={quay}/>}
          </For>
        }
      </Match>
    </Switch>
  </fieldset>;
}