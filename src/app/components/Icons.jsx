

// TODO: Move to symbols table and replace with <use/>
export function StarIcon({filled}){
  let className = ["star-icon", filled ? "filled" : "hollow"].join(" ");
  /* https://commons.wikimedia.org/wiki/File:Ic_star_outline_24px.svg */
  return <svg
    class={className}
    xmlns="http://www.w3.org/2000/svg"
    width="1em" height="1em"
    viewBox="0 0 47.95813 46.100376">
    <style>{`
path {
  fill-opacity:1;
  fill-rule:nonzero;
  stroke-width:4;
  stroke-linecap:butt;
  stroke-linejoin:round;
  stroke-miterlimit:4;
  stroke-dasharray:none;
  stroke-dashoffset:0;
  stroke-opacity:1;
  paint-order:stroke markers fill
}
  `}
    </style>
    <g
      transform="translate(-19.051331,-44.998793)">
      <path
        d="m 43.030396,49.998163 5.86503,11.883852 13.114608,1.905663 -9.48982,9.250287 2.240243,13.061617 -11.730061,-6.16686 -11.730063,6.166859 2.240244,-13.061616 -9.48982,-9.250288 13.114608,-1.905662 z"
      />
    </g>
  </svg>;
}
