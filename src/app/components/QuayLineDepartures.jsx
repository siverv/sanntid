import { createMemo, createSignal } from "solid-js";
import { useDepartureStore } from '../state';
import { getMessage, getRelativeTimeFormat } from "../i18n/i18n";
import { getClockFace, RelativeToNow } from "../../utils/time";
import { Quay } from "./Quay.jsx";
import { joinId } from '../state/quays';
import { Star } from "./QuayStar";

import "./QuayLineDepartures.css";

export function QuayLineName({quay, line, name, standalone, showQuay, toggleShowQuay}){
  return <>
    {line.publicCode ? <span class="line-number">
      {line.publicCode}
    </span> : null}
    <span class="line-name">
      <a href={`/selected?id=${encodeURIComponent(joinId(quay.id, line.id, name))}`} class="line-link">
        {name}
      </a>
    </span>
    <Show when={standalone}>
      <Show when={toggleShowQuay} fallback={
        <span class="line-quay-name">
          {quay.name}
        </span>
      }>
        <button type="button" class="line-quay-name toggle-full-quay" title={getMessage("quayMap.line.showQuay")} onclick={toggleShowQuay}>
          {quay.name}
          <span class="chevron">
            {showQuay() ? "▼" : "▶"}
          </span>
        </button>
      </Show>
    </Show>
    {line.authority ? <span class="line-authority">
      {line.authority.name}
    </span> : null}
  </>;
}

export function SingleQuayLineDepartures({quay, line, standalone, name, departuresForLine}){
  const [showQuay, setShowQuay] = createSignal(false);
  const toggleShowQuay = () => {
    setShowQuay(!showQuay());
  };
  let departures = createMemo(() => {
    let depsForLine = departuresForLine();
    if(name){
      return depsForLine.filter(deps => deps.destinationDisplay.frontText === name);
    } else return depsForLine;
  });
  let classNames = ["quay-line-departures"];
  if(standalone){
    classNames.push("standalone");
  }
  return <>
    <section class={classNames.join(" ")} style={line.presentation ? {
      "--bg": "#" + line.presentation.colour,
      "--fg": "#" + line.presentation.textColour,
    } : undefined}>
      <div class="line-header">
        <QuayLineName quay={quay} line={line} name={name} standalone={standalone} showQuay={showQuay} toggleShowQuay={toggleShowQuay}/>
        <Star quay={quay} lineId={line.id} name={name}/>
      </div>
      <div class="line-departures">
        <For each={departures()} fallback={
          <span>
            {getMessage("quayMap.line.noDepartures")}
          </span>
        }>
          {dep => {
            let expected = new Date(dep.expectedDepartureTime || dep.aimedDepartureTime);
            let aimed = new Date(dep.aimedDepartureTime);
            let classNames = ["departure-time"];
            let minutesDelay = Math.ceil((aimed - expected)/60000);
            if(minutesDelay > 3){
              classNames.push("is-delayed");
            }
            let title = createMemo(() => {
              let text = getClockFace(expected);
              if(minutesDelay > 3){
                text += `  -  ${getMessage("quayMap.line.delay")}: ${getRelativeTimeFormat().format(minutesDelay, "minutes")}`;
              }
              return text;
            });
            classNames.push(dep.realtime ? "is-realtime" : "is-static");
            return <span class={classNames.join(" ")} title={title()}>
              <RelativeToNow then={expected} tick={true}/>
            </span>;
          }}
        </For>
      </div>
      <Show when={showQuay()}>
        <section class="temporary-full-quay-section">
          <Quay quay={quay}/>
        </section>
      </Show>
    </section>
  </>;
}

export function QuayLineDepartures(props){
  let [quayLineDepartureMap] = useDepartureStore();
  let departuresForLine = createMemo(()=>{
    let map = quayLineDepartureMap();
    let lineMap = map?.get(props.quay.id);
    return lineMap?.get(props.line.id) || [];
  });
  let frontTexts = createMemo(() => {
    let depsForLine = departuresForLine();
    if(depsForLine.length === 0){
      return [props.line.name + (props.line.description  ? ` - ${props.line.description} ` : '')];
    }
    return Array.from(new Set(depsForLine.map(d => d.destinationDisplay.frontText)));
  });
  if(props.name){
    return <SingleQuayLineDepartures {...props} departuresForLine={departuresForLine}/>;
  }
  return <For each={frontTexts()}>
    {frontText => <SingleQuayLineDepartures {...props} name={frontText} departuresForLine={departuresForLine}/>}
  </For>;
}
