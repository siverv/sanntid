import { useQuayMap, useDepartureStore } from '../state';
import { getMessage } from "../i18n/i18n.js";
import { getClockFace, RelativeToNow } from "../../utils/time.jsx";
import "./LastUpdatedStatusBar.css";

export function LastUpdatedStatusBar(){
  const [,, lastUpdated] = useQuayMap();
  const [depMap, {refetch, error}] = useDepartureStore();
  return <>
    <span class="status-bar last-updated">
      <span class="label">
        <Switch>
          <Match when={depMap.loading}>
            <i>{getMessage("quayMap.lastUpdated.loading")}</i>
          </Match>
          <Match when={lastUpdated() <= 0 || isNaN(lastUpdated())}>
            <i>{getMessage("quayMap.lastUpdated.neverBefore")}</i>
          </Match>
          <Match when={lastUpdated()}>
            {() => <>
              <span class="pretext">
                {getMessage("quayMap.lastUpdated.lastUpdated")}{`: `}
              </span>
              <span class="timestamp" title={getClockFace(lastUpdated())}>
                <RelativeToNow then={lastUpdated()}/>
              </span>
            </>}
          </Match>
        </Switch>
      </span>
      <button onClick={() => refetch()} disabled={depMap.loading || isNaN(lastUpdated())}>
        {getMessage("quayMap.lastUpdated.refresh")}
      </button>
    </span>
    <Show when={error()}>
      {specificError => <section class="status-bar error">
        <span class="label">
          {getMessage("quayMap.error.fetching")}: {specificError.toString()}
        </span>
      </section>}
    </Show>
  </>;
}


