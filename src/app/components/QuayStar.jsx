import { createMemo } from "solid-js";
import { useSaved } from "../state";
import { getMessage } from "../i18n/i18n";
import { StarIcon } from "./Icons";
import "./QuayStar.css";

export function Star({quay, lineId, name}){
  const [_getSaved, _setSaved, {isSaved, toggleSaved}] = useSaved();
  const title = createMemo(()=> {
    if(isSaved(quay.id, lineId, name)){
      return getMessage("quayMap.quayStar.toggle.isSaved");
    } else {
      return getMessage("quayMap.quayStar.toggle.isNotSaved");
    }
  });
  return <>
    <label class="quay-star" title={title()}>
      <input
        class="quay-star-checkbox"
        type="checkbox"
        title={getMessage("quayMap.quayStar.checkbox") + (name ? ": " + name : "")}
        id={"starred-quay--"+quay.id}
        checked={isSaved(quay.id, lineId, name) ? "yes" : undefined}
        onInput={() => toggleSaved(quay, lineId, name)}
      />
      <span class="quay-star-icon">
        <Show when={isSaved(quay.id, lineId, name)}>
          <StarIcon filled={true}/>
        </Show>
        <Show when={!isSaved(quay.id, lineId, name)}>
          <StarIcon filled={false}/>
        </Show>
      </span>
    </label>
  </>;
}
