import { Switch, Match, createMemo } from "solid-js";
import { useGeolocation } from "../../utils/geolocation";
import { getMessage } from "../i18n/i18n";
import { getClockFace, RelativeToNow } from "../../utils/time.jsx";
import "./GeolocationStatusBar.css";

export function UseGeolocation({showCoordinates}){
  let {coords, error, permission} = useGeolocation();
  return <Switch fallback={<i>{getMessage("geolocation.loading")}</i>}>
    <Match when={permission() === false}>
      {getMessage("geolocation.denied")}
    </Match>
    <Match when={error() != null}>
      {getMessage("geolocation.error")}: <pre>
        {error().message || error().toString()}
      </pre>
    </Match>
    <Match when={coords() != null}>
      <Show when={showCoordinates} fallback={<></>}>
        <span class="geo-lat">
          {getMessage("geolocation.lat")}: {coords().latitude}
        </span>
        <br/>
        <span class="geo-long">
          {getMessage("geolocation.long")}: {coords().longitude}
        </span>
      </Show>
    </Match>
  </Switch>;
}


export function GeolocationStatusBar(){
  let {timestamp, coords, error, permission, refetch} = useGeolocation();
  return <span class="status-bar geo-status">
    <span class="label">
      <Switch fallback={<i>{getMessage("geolocation.loading")}</i>}>
        <Match when={permission() === false}>
          {getMessage("geolocation.denied")}
        </Match>
        <Match when={error() != null}>
          {getMessage("geolocation.error")}: <pre>
            {error().message || error().toString()}
          </pre>
        </Match>
        <Match when={coords() != null && timestamp()}>
          {() => {
            return <>
              <span>
                {getMessage("geolocation.lastUpdated")}{`: `}
              </span>
              <span title={getClockFace(new Date(timestamp()))}>
                <RelativeToNow then={new Date(timestamp())}/>
              </span>
            </>;
          }}
        </Match>
      </Switch>
    </span>
    <Show when={coords()} fallback={<></>}>
      <span class="geo-coords">
        <span class="geo-lat">
          {getMessage("geolocation.lat")}: {coords().latitude}
        </span>
        <span class="geo-long">
          {getMessage("geolocation.long")}: {coords().longitude}
        </span>
      </span>
    </Show>
    <button onclick={refetch} disabled={coords() === null && error() === null && permission() === null}>
      {getMessage("geolocation.refresh")}
    </button>
  </span>;
}