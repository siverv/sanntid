import { useQuayMap } from '../state';
import { TransportMode } from './TransportMode';
import { Quay } from './Quay';

function getQuayListForStopPlaceId(stopPlaceId, quayMap){
  return Array.from([...quayMap().values()])
    .filter(quay => quay.stopPlace.id === stopPlaceId);
}

function getStopPlaces(quayMap){
  return Array.from(
    Array.from(quayMap().values())
      .reduce(
        (map, quay) =>
          map.set(quay.stopPlace.id, {...quay.stopPlace, quay}),
        new Map()
      )
      .values()
  );
}

export function StopPlaceName({stopPlace}){
  let modes = new Set(stopPlace.quay.lines.flatMap(line => line.transportMode));
  return <>
    {stopPlace.name}
    {stopPlace.description ? <> - {stopPlace.description}</> : null}
    <Show when={modes.size > 0}>
      <span> - </span>
      <For each={Array.from(modes)}>
        {mode => <TransportMode mode={mode}/>}
      </For>
    </Show>
  </>;
}

export function DisplayStopPlace({stopPlace}){
  const [quayMap] = useQuayMap();
  return <section class="stop-place">
    <h3 id={stopPlace.id}>
      <StopPlaceName stopPlace={stopPlace}/>
    </h3>
    <div>
      <For each={getQuayListForStopPlaceId(stopPlace.id, quayMap)}>
        {quay => <Quay quay={quay}/>}
      </For>
    </div>
  </section>;
}

export function GroupQuaysByStopPlace(){
  let [quayMap] = useQuayMap();
  return <For each={[...getStopPlaces(quayMap).values()]}>
    {(stopPlace) => <DisplayStopPlace stopPlace={stopPlace}/>}
  </For>;
}
