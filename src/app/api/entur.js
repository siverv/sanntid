import createEnturService from '@entur/sdk';

import gql_quaysByStopPlaces from './query/quaysByStopPlaces.graphql?raw';
import gql_callByQuay from './query/callByQuay.graphql?raw';
import gql_quaysByIds from './query/quaysByIds.graphql?raw';
import { useGeolocation } from '../../utils/geolocation';
import { addDebugLine } from '../../utils/DebugLog';

/**
 * @typedef StopPlace
 * @type {object}
 * @property {string} id
 * @property {string} name
 * @property {string} [description]
 * @property {StopPlace} [parent]
 * 
 */

/**
 * @typedef Line
 * @type {object}
 * @property {string} id
 * @property {string} name
 * @property {string} publicCode
 * @property {string} [description]
 * @property {string} transportMode
 * @property {string} transportSubmode
 * @property {{id: string, name: string}} authority
 * @property {{textColour: string, colour: string}} presentation
 * @property {{id: string, publicCode: string, text: string}[]} notices
 * 
 */

/**
 * @typedef Quay
 * @type {object}
 * @property {string} id
 * @property {string} name
 * @property {string} publicCode
 * @property {string} description
 * @property {number} longitute
 * @property {number} latitude
 * @property {StopPlace} stopPlace
 * @property {Line[]} lines
 * 
 */

const ET_CLIENT_NAME = import.meta.env.VITE_SANNTID_ENTUR_CLIENT_NAME;

if(!ET_CLIENT_NAME){
  const errorMessage = "ET-Client-Name is required. Please set the VITE_SANNTID_ENTUR_CLIENT_NAME env-variable to an appropriate value";
  document.body.innerText = errorMessage;
  throw new Error(errorMessage);
}



async function queryJourneyPlanner(query, variables){
  const response = await fetch("https://api.entur.io/journey-planner/v3/graphql", {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      "ET-Client-Name": ET_CLIENT_NAME
    },
    body: JSON.stringify({
      query,
      variables
    })
  });
  if(response.status >= 400) {
    console.warn("Query Journal Planner Failed", response, await response.text());
    return [];
  }
  const result = await response.json();
  return result.data;
}


export const service = createEnturService({
  clientName: ET_CLIENT_NAME,
});

export async function venueSuggestions(value, {value: previousSuggestions}){
  if(value.length < 3){
    return previousSuggestions;
  } else {
    let {coords} = useGeolocation();
    let features = await service.getFeatures(value, coords(), {
      limit: 10,
      layers: ["venue"]
    });
    return features.map(feat => {
      return {
        id: feat.properties.id,
        label: feat.properties.label,
        tags: feat.properties.category,
        feature: feat,
      };
    });
  }
}

/**
 * Fetch nearby feature-ids by coordinates
 * @param {{longitude: number, latitude: number}} coordinates
 * @return {string[]}
 */
export async function getNearbyStopPlaceIds (coordinates) {
  let features = await service.getFeaturesReverse(coordinates, {layers: ["venue"]});
  return Array.from(new Set(features.map(f => f.properties.id)));
}


/**
 * Fetch list of quays by StopPlace ids.
 * @param {string[]} stopPlaceIds
 * @return {Quay[]}
 */
export async function fetchQuaysByStopPlaceIds(stopPlaceIds){
  let data = await queryJourneyPlanner(gql_quaysByStopPlaces, {
    ids: stopPlaceIds
  });
  let quays = data.stopPlaces.flatMap(stopPlace => stopPlace.quays);
  return quays;
}


/**
 * Fetch list of quays by their ids
 * @param {string[]} quayIds
 * @return {Quay[]}
 */
export async function fetchQuaysByIds(quayIds){
  let data = await queryJourneyPlanner(gql_quaysByIds, {
    ids: quayIds
  });
  return data.quays;
}



function batchify(fn){
  const MIN_REQUEST_INTERVAL = 5 * 1000;
  let previousRequestTime = new Date() - MIN_REQUEST_INTERVAL;
  let queueBatchTimeoutId;
  let resolveBatch = new Map();

  async function fetchBatchRequest(){
    let oldResolveBatch = resolveBatch;
    resolveBatch = new Map();
    let idSet = new Set(Array.from(oldResolveBatch.keys()).flatMap(list => list));
    previousRequestTime = new Date();
    let data = await fn(Array.from(idSet));
    for(let [quayIds, {resolve}] of oldResolveBatch) {
      let result = new Map(
        quayIds.filter(id => data.has(id))
          .map(id => [id, data.get(id)])
      );
      resolve(result);
    }
  }
  
  function queueBatchRequest(){
    if(queueBatchTimeoutId){
      return;
    }
    let timeUntilOk = MIN_REQUEST_INTERVAL - (Date.now() - previousRequestTime);
    queueBatchTimeoutId = setTimeout(() => {
      queueBatchTimeoutId = null;
      fetchBatchRequest();
    }, timeUntilOk);
  }

  return async (quayIds) => {
    if(queueBatchTimeoutId || Date.now() - previousRequestTime < MIN_REQUEST_INTERVAL){
      return await new Promise((resolve, reject) => {
        resolveBatch.set(quayIds, {resolve, reject});
        queueBatchRequest();
      });
    } else {
      previousRequestTime = new Date();
      return await fn(quayIds);
    }
  };
}

export const getDeparturesFromQuayIds = batchify(async (quayIds) => {
  addDebugLine("Fetching call for quays: " + quayIds.join(", "));
  let data = await queryJourneyPlanner(gql_callByQuay, {
    ids: quayIds,
    startDate: (new Date()).toISOString()
  });
  return data.quays.reduce((map, quay) => {
    return map.set(quay.id, quay.estimatedCalls.reduce((lineMap, call) => {
      let lineId = call.serviceJourney.journeyPattern.line.id;
      return lineMap.set(lineId, (lineMap.get(lineId)||[]).concat(call));
    }, new Map()));
  }, new Map());
});
