import { batch, createSignal } from 'solid-js';
import { UseGeolocation } from "../../app/components/GeolocationStatusBar";
import { ModifySaved } from "./ModifySaved";
import { useSaved, useConfig } from '../../app/state';
import { getMessage, language, setLanguage, languages } from "../../app/i18n/i18n";
import { CopyToClipboardButton } from '../../utils/CopyToClipboardButton';
import { DebugLevelInput } from '../../utils/DebugLog';

function selectLanguage(language){
  localStorage.setItem("userPreferredLanguage", language);
  setLanguage(language);
}

function exportSettings(configGetters, getSavedMap){
  const settings = {
    userPreferredLanguage: language(),
    autoTick: Boolean(configGetters.hasAutoTick()),
    showExactTime: Boolean(configGetters.showExactTime()),
    showAllLines: Boolean(configGetters.showAllLines()),
    favourites: Array.from(getSavedMap())
  };
  const serializedSettings = JSON.stringify(settings);
  return serializedSettings;
}

function importSettings(serializedSettings, configSetters, setSavedMap){
  const settings = JSON.parse(serializedSettings);
  if(!settings.favourites){
    throw new Error("No favourites among the settings");
  }
  batch(() => {
    selectLanguage(String(settings.userPreferredLanguage));
    configSetters.setAutoTick(Boolean(settings.autoTick));
    configSetters.setShowExactTime(Boolean(settings.showExactTime));
    configSetters.setShowAllLines(Boolean(settings.showAllLines));
    setSavedMap(new Map(settings.favourites));
  });
}

function ImportSettings(){
  const [_configGetters, configSetters] = useConfig();
  const [_getSavedMap, setSavedMap] = useSaved();
  const [notes, setNotes] = createSignal(null);
  function onSubmit(ev){
    ev.preventDefault();
    ev.stopPropagation();
    try {
      let serializedSettings = new FormData(ev.target).get("import-settings");
      importSettings(serializedSettings, configSetters, setSavedMap);
      setNotes({type: "info", content: "Settings imported!"});
    } catch(err){
      return setNotes({type: "error", content: "Could not import settings: " + err});
    }
  }
  return <form onSubmit={onSubmit}>
    <textarea name="import-settings"/>
    <button>
      {getMessage("settings.import.button")}
    </button>
    <Show when={notes()}>
      {({type, content}) => <span class={"notes " + type}>{content}</span>}
    </Show>
  </form>;
}


function ExportSettings(){
  const [configGetters] = useConfig();
  const [getSavedMap] = useSaved();
  const [getSerializedSettings, setSerializedSettings] = createSignal();
  return <div>
    <button type="button" onClick={() => setSerializedSettings(exportSettings(configGetters, getSavedMap))}>
      {getMessage("settings.export.button")}
    </button>
    <textarea class="export-settings" disabled={!getSerializedSettings()}>{getSerializedSettings()}</textarea>
    <Show when={getSerializedSettings()}>
      <CopyToClipboardButton getValue={getSerializedSettings}/>
    </Show>
  </div>;
}


export default function SettingsView(){
  const [configGetters, configSetters] = useConfig();
  const [_get, _set, {clearAllSaved}] = useSaved();
  return <section id="settings">
    <h3>
      {getMessage("settings.header")}
    </h3>
    <fieldset>
      <legend>
        {getMessage("settings.preferences")}
      </legend>
      <label>
        {getMessage("settings.selectPreferredLanguage")}
        <select value={language()} onBlur={(ev) => selectLanguage(ev.target.value)}>
          <For each={Array.from(languages.entries())}>
            {([langCode, langName]) => <option value={langCode}>{langName}</option>}
          </For>
        </select>
      </label>
      <label>
        {getMessage("settings.autoTick")}
        <input type="checkbox" name="autoTick" checked={configGetters.hasAutoTick()} onInput={(ev) => configSetters.setAutoTick(ev.target.checked)}/>
      </label>
      <label>
        {getMessage("settings.showExactTime")}
        <input type="checkbox" name="showExactTime" checked={configGetters.showExactTime()} onInput={(ev) => configSetters.setShowExactTime(ev.target.checked)}/>
      </label>
      <label>
        {getMessage("settings.showAllLines")}
        <input type="checkbox" name="showExactTime" checked={configGetters.showAllLines()} onInput={(ev) => configSetters.setShowAllLines(ev.target.checked)}/>
      </label>
    </fieldset>
    <fieldset>
      <legend>
        {getMessage("settings.geolocation")}
      </legend>
      <UseGeolocation showCoordinates/>
    </fieldset>
    <fieldset>
      <legend>
        {getMessage("settings.system")}
      </legend>
      <div>
        <button onClick={() => {
          let ok = confirm(getMessage("settings.clearPermanentStorage.confirm"));
          if(ok){
            clearAllSaved();
            localStorage.clear();
          }
        }}>
          {getMessage("settings.clearPermanentStorage")}
        </button>
      </div>
      <hr/>
      <div>
        <button onClick={() => window.location.reload()}>
          {getMessage("settings.hardReload")}
        </button>
      </div>
      <hr/>
      <DebugLevelInput/>
    </fieldset>
    <fieldset>
      <legend>
        {getMessage("settings.export")}
      </legend>
      <ExportSettings/>
    </fieldset>
    <fieldset>
      <legend>
        {getMessage("settings.import")}
      </legend>
      <ImportSettings/>
    </fieldset>
    <fieldset>
      <legend>
        {getMessage("settings.saved")}
      </legend>
      <ModifySaved/>
    </fieldset>
    <fieldset>
      <legend>
        {getMessage("settings.departures")}
      </legend>
      <div class="quay-line-departures">
        <div class="line-departures">
          <span class={"departure-time is-realtime"}>
            {getMessage("settings.departures.realtime")}
          </span>
          <span class={"departure-time is-static"}>
            {getMessage("settings.departures.static")}
          </span>
          <span class={"departure-time is-delayed"}>
            {getMessage("settings.departures.delayed")}
          </span>
        </div>
      </div>
    </fieldset>
  </section>;
}
