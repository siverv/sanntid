import { createMemo } from "solid-js";
import { QuayName } from "../../app/components/Quay";
import { QuayLineName } from "../../app/components/QuayLineDepartures";
import { useDepartureStore, useSaved } from '../../app/state';
import { QuayMapContext, createQuayStore, splitId } from '../../app/state/quays';
import { getMessage } from "../../app/i18n/i18n";

function MovableQuayRow({id, index, length, quay}){
  const [_getMap, _setMap, {isSaved, moveSaved, toggleSavedById}] = useSaved();
  let [, lineId, name] = splitId(id);
  let getLine = createMemo(() => lineId ? quay.lines.find(line => line.id === lineId) : null);
  const getName = createMemo(() => {
    let line = getLine();
    if(!name && line){
      return line.name + (line.description  ? ` - ${line.description} ` : '');
    } else return name;
  });
  return <tr>
    <td>
      <button onClick={() => moveSaved(id, index() - 1)} disabled={index() <= 0}>{getMessage("modifySaved.up")}</button>
    </td>
    <td>
      <button onClick={() => moveSaved(id, index() + 1)} disabled={index() >= length -1}>{getMessage("modifySaved.down")}</button>
    </td>
    <td>
      <Show when={getLine()} fallback={<QuayName quay={quay}/>}>
        {line => {
          return <div class="quay-line-departures" style={line.presentation ? {
            "--bg": "#" + line.presentation.colour,
            "--fg": "#" + line.presentation.textColour,
          } : undefined}>
            <span class="line-header">
              <QuayLineName quay={quay} line={line} name={getName}/>
            </span>
          </div>;
        }}
      </Show>
    </td>
    <td>
      <button onClick={() => toggleSavedById(id, quay)}>
        <Show when={isSaved(id)} fallback={getMessage("modifySaved.restore")}>{getMessage("modifySaved.remove")}</Show>
      </button>
    </td>
  </tr>;
}


export function ModifySaved(){
  const departureStore = useDepartureStore();
  const [getSaved, setSaved] = useSaved();
  const quayStore = createQuayStore(departureStore, [getSaved, setSaved]);
  return <section id="saved" class="modify-saved">
    <QuayMapContext.Provider value={quayStore}>
      <table>
        <tbody>
          <For each={Array.from(getSaved().keys())} fallback={"..."}>
            {(id, index) => {
              let quay = getSaved().get(id);
              return <MovableQuayRow id={id} index={index} length={getSaved().size} quay={quay}/>;
            }}
          </For>
        </tbody>
      </table>
    </QuayMapContext.Provider>
    {/*<footer>
      <a href={"/saved"}>{getMessage("saved.back")}</a>
    </footer>*/}
  </section>;
}
