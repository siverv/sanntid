import { GroupQuaysByStopPlace } from "../../app/components/StopPlace";
import { LastUpdatedStatusBar } from "../../app/components/LastUpdatedStatusBar";
import { GeolocationStatusBar } from '../../app/components/GeolocationStatusBar';
import { getNearbyStopPlaceIds, fetchQuaysByStopPlaceIds } from "../../app/api/entur";
import { createLocalStorageMapSignal } from "../../utils/mapSignal";
import { useGeolocation } from "../../utils/geolocation";
import { getMessage } from "../../app/i18n/i18n";
import { useDepartureStore } from '../../app/state';
import { QuayMapContext, createQuayStore } from '../../app/state/quays';

async function loadNearbyQuays([_quayMap, setQuayMap], coords){
  if(!coords()){
    return;
  }
  let stopPlaceIds = await getNearbyStopPlaceIds(coords());
  let quays = await fetchQuaysByStopPlaceIds(stopPlaceIds);
  setQuayMap(new Map(quays.map(q => [q.id, q])));
}

export default function NearbyPage(){
  const departureStore = useDepartureStore();
  const signal = createLocalStorageMapSignal("nearby");
  const quayStore = createQuayStore(departureStore, signal);
  let {coords} = useGeolocation();
  return <section id="nearby">
    <QuayMapContext.Provider value={quayStore}>
      <LastUpdatedStatusBar/>
      <GeolocationStatusBar/>
      <button onClick={() => loadNearbyQuays(signal, coords)}>
        {getMessage("nearby.loadNearby")}
      </button>
      <GroupQuaysByStopPlace/>
    </QuayMapContext.Provider>
    <Show when={signal[0]().size > 0}>
      <button onclick={() => signal[1](new Map())}>
        {getMessage("common.clearData")}
      </button>
    </Show>
  </section>;
}
