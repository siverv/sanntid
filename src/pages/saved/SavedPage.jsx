import { For, createMemo } from "solid-js";
import { Quay } from "../../app/components/Quay";
import { LastUpdatedStatusBar } from "../../app/components/LastUpdatedStatusBar";
import { QuayLineDepartures } from "../../app/components/QuayLineDepartures";
import { useDepartureStore, useSaved } from '../../app/state';
import { QuayMapContext, createQuayStore, splitId } from '../../app/state/quays';
import { getMessage } from "../../app/i18n/i18n";

export default function SavedPage(){
  const departureStore = useDepartureStore();
  const [getSaved, setSaved] = useSaved();
  const quayStore = createQuayStore(departureStore, [getSaved, setSaved]);
  const shareParams = createMemo(() => {
    let ids = Array.from(getSaved().keys());
    let params = new URLSearchParams();
    ids.forEach(id => params.append("id", encodeURIComponent(id)));
    return params.toString();
  });
  return <section id="saved">
    <QuayMapContext.Provider value={quayStore}>
      <LastUpdatedStatusBar/>
      <div>
        <For each={Array.from(getSaved().keys())} fallback={"..."}>
          {id => {
            let quay = getSaved().get(id);
            let [,lineId, name] = splitId(id);
            if(lineId){
              let line = quay.lines.find(line => line.id === lineId);
              return <QuayLineDepartures quay={quay} line={line} name={name} standalone={true}/>;
            } else {
              return <Quay quay={quay}/>;
            }
          }}
        </For>
      </div>
    </QuayMapContext.Provider>
    <footer>
      <a href={`/selected?${shareParams()}`}>{getMessage("saved.share")}</a>
    </footer>
  </section>;
}
