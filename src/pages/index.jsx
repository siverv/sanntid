import { Route } from "@solidjs/router";
import SearchPage from "./search/SearchPage";
import NearbyPage from "./nearby/NearbyPage";
import SettingsPage from "./settings/SettingsPage";
import SelectedPage from "./selected/SelectedPage";
import SavedPage from "./saved/SavedPage";

export const pages = [
  <Route path="/selected" component={SelectedPage}/>,
  <Route path="/nearby" component={NearbyPage}/>,
  <Route path="/search" component={SearchPage}/>,
  <Route path="/settings" component={SettingsPage}/>,
  <Route path="/saved" component={SavedPage}/>,
  <Route path="*" component={SavedPage}/>,
];