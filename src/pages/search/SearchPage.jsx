import { BySearch } from './BySearch';
import { createLocalStorageMapSignal } from "../../utils/mapSignal";
import { getMessage } from "../../app/i18n/i18n";
import { useDepartureStore } from '../../app/state';
import { QuayMapContext, createQuayStore } from '../../app/state/quays';


export default function SearchPage(){
  const departureStore = useDepartureStore();
  const signal = createLocalStorageMapSignal("search");
  const quayStore = createQuayStore(departureStore, signal);
  return <section id="search">
    <QuayMapContext.Provider value={quayStore}>
      <BySearch/>
    </QuayMapContext.Provider>
    <Show when={signal[0]().size > 0}>
      <button onclick={() => signal[1](new Map())}>
        {getMessage("common.clearData")}
      </button>
    </Show>
  </section>;
}
