import { GroupQuaysByStopPlace } from "../../app/components/StopPlace";
import { TransportMode } from "../../app/components/TransportMode";
import { LastUpdatedStatusBar } from "../../app/components/LastUpdatedStatusBar";
import { SearchAsYouType } from "../../utils/SearchAsYouType";
import { fetchQuaysByStopPlaceIds, venueSuggestions } from "../../app/api/entur";
import { useQuayMap } from '../../app/state';

export function VenueTag({tag}){
  switch(tag){
  case "onstreetBus":
    return <TransportMode mode="bus"/>;
  case "metroStation":
    return <TransportMode mode="metro"/>;
  default:
    return null;
  }
}

export function saveQuaysForVenue(venue, setQuayMap){
  fetchQuaysByStopPlaceIds([venue.id])
    .then(quays => {
      setQuayMap(
        new Map(
          quays.map(q => [q.id, q])
        )
      );
    });
}

export function VenueSuggestion({item: venue}){
  let [_, setQuayMap] = useQuayMap();
  const action = () => {
    saveQuaysForVenue(venue, setQuayMap);
  };
  return <li><button class="venue-suggestion" onClick={() => action()} title={venue.id}>
    {venue.label}
    <For each={venue.tags}>
      {tag => <VenueTag tag={tag}/>}
    </For>
  </button></li>;
}


export function BySearch(){
  return <>
    <LastUpdatedStatusBar/>
    <SearchAsYouType Item={VenueSuggestion} search={venueSuggestions}/>
    <GroupQuaysByStopPlace/>
  </>;
}
