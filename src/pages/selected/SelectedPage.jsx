import { Quay } from "../../app/components/Quay";
import { LastUpdatedStatusBar } from "../../app/components/LastUpdatedStatusBar";
import { QuayLineDepartures } from "../../app/components/QuayLineDepartures";
import { getMessage } from "../../app/i18n/i18n";
import { createResource } from 'solid-js';
import { fetchQuaysByIds } from '../../app/api/entur';
import { useLocation } from "@solidjs/router";
import { useDepartureStore } from '../../app/state';
import { QuayMapContext, createQuayStore, splitId } from '../../app/state/quays';

function createEphemeralQuayMapSignal(ids){
  const [quayMap, {mutate}] = createResource(async () => {
    const quayIds = ids.map(id => splitId(id)[0]);
    const quays = await fetchQuaysByIds(quayIds);
    let map = new Map();
    for(let id of ids){
      let [quayId] = splitId(id);
      let quay = quays.find(quay => quay.id === quayId);
      map.set(id, quay);
    }
    return map;
  });
  return [quayMap, mutate];
}

export default function SelectedPage(){
  const location = useLocation();
  const searchParams = new URLSearchParams(location.search);
  const ids = searchParams.getAll("id").map(id => decodeURIComponent(id));
  const departureStore = useDepartureStore();
  const [getQuayMap, setQuayMap] = createEphemeralQuayMapSignal(ids);
  return <section id="selected">
    <Show when={getQuayMap()} fallback={<>Loading....</>}>
      {() => {
        const quayStore = createQuayStore(departureStore, [getQuayMap, setQuayMap]);
        return <QuayMapContext.Provider value={quayStore}>
          <LastUpdatedStatusBar/>
          <div>
            <For each={Array.from(getQuayMap().keys())} fallback={"..."}>
              {id => {
                let quay = getQuayMap().get(id);
                let [,lineId, name] = splitId(id);
                if(lineId){
                  let line = quay.lines.find(line => line.id === lineId);
                  return <QuayLineDepartures quay={quay} line={line} name={name} standalone={true}/>;
                } else {
                  return <Quay quay={quay}/>;
                }
              }}
            </For>
          </div>
        </QuayMapContext.Provider>;
      }}
    </Show>
    <footer>
      <a href={`/selected?${location.search}`}>{getMessage("selected.share")}</a>
    </footer>
  </section>;
}
