/* @refresh reload */
import { render } from 'solid-js/web';
import { HashRouter } from "@solidjs/router";
import {registerPWA} from './pwa';
import {App} from './app/App';
import {pages} from "./pages";

import "./main.css";

render(() =>
  <HashRouter root={App}>
    {pages}
  </HashRouter>, document.getElementById('root'));
registerPWA();
