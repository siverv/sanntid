import {createSignal} from 'solid-js';
import { registerSW } from 'virtual:pwa-register';
const [_updateReady, setUpdateReady] = createSignal(null);
const [_offlineReady, setOfflineReady] = createSignal(false);

export function registerPWA(){
  const _updateSW = registerSW({
    onNeedRefresh() {
      setUpdateReady({
        applyUpdate: () => {
          setUpdateReady(null);
          _updateSW();
        }
      });
    },
    onOfflineReady() {
      setOfflineReady(true);
    },
  });
}


export const updateReady = _updateReady;
export const offlineReady = _offlineReady;