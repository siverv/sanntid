import { defineConfig } from "vite";
import solidPlugin from "vite-plugin-solid";
import { VitePWA } from 'vite-plugin-pwa';
import {execSync} from "child_process";

const commitHash = execSync('git rev-parse --short HEAD');

export default defineConfig({
  plugins: [
    solidPlugin(),
    VitePWA({
      manifest: {
        "name": "Sanntid Rutetabell",
        "short_name": "Sanntid",
        "display": "standalone",
        "background_color": "#4682b4",
        "description": "Enkel sanntid rutetabell basert på Enturs API"
      }
    })
  ],
  define: {
    // eslint-disable-next-line no-undef
    '__APP_VERSION__': JSON.stringify(process.env.npm_package_version),
    '__COMMIT_HASH__': JSON.stringify(commitHash.toString()),
  },
  build: {
    target: "esnext",
    polyfillDynamicImport: false,
  }
});
